# stem-yt

Prosta strona internetowa komunikująca się z API YouTuba, przez co możemy pobierać dane o kanałach

## Prosty poradnik jak postawić
* Pobierz moment-with-locales.js ze strony http://momentjs.com/, zmień nazwę na moment.js i umieść w tym katalu
* Wyśmigaj klucz API yt i wpisz go w odpowiednim miejscu w main.js (zmienna key nad funkcją zaladuj, ok 20 linijka)

[Demo](https://papryka.pro/yt/)
* Różnice między tym source a demem: brak reklamy programu i ostatnich wyszukiwań