window.onload = function() {
  var wyszukane = false;
  var ponazwie = document.getElementById("ponazwie");
  var poid = document.getElementById("poid");
  ponazwie.addEventListener("keyup", function(event) {
    if (event.key === "Enter" && ponazwie.value != "") {
      nazwa(ponazwie.value);
      // zaladuj(ponazwie.value, 0, 0);
    }
  });
  poid.addEventListener("keyup", function(event) {
    if (event.key === "Enter" && poid.value != "") {
      // animujPojawienie();
      zaladuj(poid.value, 1, 0);
    }
  });
  // feczuj();
}

let key = 'AIzaSyAaPP6PCF_kh8twixLYzkLvXy3YPB5sqcw';
function zaladuj(nazwa, typ, anon) {
  if(typ == 0) {
    var wzorzec = "forUsername=";
  } else if(typ == 1){
    var wzorzec = "id=";
  }
    fetch('https://www.googleapis.com/youtube/v3/channels?key='+key+'&part=snippet,contentDetails,statistics&fields=items(id,snippet(title,description,customUrl,publishedAt,thumbnails(default(url)),country),statistics,contentDetails)&'+wzorzec+nazwa)
  .then(function(response) {
    return response.json();
  })
  .then(function(jsonk) {
    // jeżeli kanału nie ma, wróć
    if(typeof jsonk.items[0] == 'undefined') {
      pokazBlad();
      // alert("Nie znaleziono takiego kanału :(");
      return;
    };
    animujPojawienie();
    // if(anon != 1) ostatnio(jsonk.items[0].id, jsonk.items[0].snippet.title, jsonk.items[0].statistics.subscriberCount);
    // usun event listenery
    zresetuj('link'); zresetuj('polub'); zresetuj('przeslane');

    document.getElementById("nazwa").innerHTML = jsonk.items[0].snippet.title; // nazwa kanalu
    document.getElementById("obrazek").src = jsonk.items[0].snippet.thumbnails.default.url; // awatar
    document.getElementById("opis").innerHTML = jsonk.items[0].snippet.description; // opis
    
    var godzinka = moment(jsonk.items[0].snippet.publishedAt) // godzina założenia ze stringa z api
    document.getElementById("data").innerHTML = moment(godzinka).locale('pl').format('LLL'); // ustaw
    
    // id i custom url
    document.getElementById("zaw-id").innerHTML = jsonk.items[0].id;

    var zawUrl = document.getElementById("zaw-url");
    var zawUrlC = document.getElementsByClassName("zaw-url")[0]

    if(typeof jsonk.items[0].snippet.customUrl != 'undefined') {
      zawUrlC.style.display = 'block';
      zawUrl.innerHTML = jsonk.items[0].snippet.customUrl;
    } else {
      zawUrlC.style.display = 'none';
    }
    // kraj kanalu
    var kraj = document.getElementById("kraj"); var krajc = document.getElementsByClassName("kraj")[0];

    if(typeof jsonk.items[0].snippet.country == 'undefined') { krajc.style.display = 'none' }
    else { krajc.style.display = 'block'; kraj.innerHTML = jsonk.items[0].snippet.country; }
    
    // statystyki
    document.getElementById("wysw").innerHTML = Number(jsonk.items[0].statistics.viewCount).toLocaleString(); // wyswietlenia
    document.getElementById("subs").innerHTML = Number(jsonk.items[0].statistics.subscriberCount).toLocaleString(); // suby
    document.getElementById("filmy").innerHTML = jsonk.items[0].statistics.videoCount; // liczba filmow

    document.getElementById("link").addEventListener('click', () => { // otwieranie kanalu w nowej karcie
      window.open(
        "https://youtube.com/channel/"+jsonk.items[0].id,
        '_blank'
      )
    })
    var polub = document.getElementById("polub"); // playlista z polubieniami
    if(typeof jsonk.items[0].contentDetails.relatedPlaylists.likes != 'undefined') {
      polub.style.display = 'block'; 
      polub.addEventListener('click', () => {
        window.open(
          "https://youtube.com/playlist?list="+jsonk.items[0].contentDetails.relatedPlaylists.likes,
          '_blank'
        )
      })
    } else {
      polub.style.display = 'none';
    }
    var przeslane = document.getElementById("przeslane"); // playlista z polubieniami
    
    if(typeof jsonk.items[0].contentDetails.relatedPlaylists.uploads != 'undefined') {
      przeslane.style.display = 'block';
      przeslane.addEventListener('click', () => {
        window.open(
          "https://youtube.com/playlist?list="+jsonk.items[0].contentDetails.relatedPlaylists.uploads,
          '_blank'
        )
      })

    } else {
      przeslane.style.display = 'none';
    }
  });
}
function wroc() {
  var lew = document.getElementsByClassName("lew")[0];
  var prawy = document.getElementsByClassName("prawy")[0];
  lew.style.display = 'block';
  lew.style.opacity = 0;
  var id = setInterval(frame, 15);
  var op = 0;
  var po = 1
  function frame() {
    if (op > 1 || po == 0) {
      clearInterval(id);
    } else {
      po-=0.1;
      op+=0.1;
      lew.style.opacity = op;
      prawy.style.opacity = po;
    }
  }
  prawy.style.display = 'none';

}
function animujPojawienie() {
  var lew = document.getElementsByClassName("lew")[0];
  var prawy = document.getElementsByClassName("prawy")[0];

  var id = setInterval(frame, 15);
  var op = 0;
  var po = 1

  prawy.style.display = 'block';
  prawy.style.opacity = 0;
  function frame() {
    if (op > 1 || po == 0) {
      clearInterval(id);
    } else {
      po-=0.1;
      op+=0.1;
      prawy.style.opacity = op;
      lew.style.opacity = po;
    }
  }
  lew.style.display = 'none';
}
function zresetuj(id) {
  var el = document.getElementById(id),
  elClone = el.cloneNode(true);
  el.parentNode.replaceChild(elClone, el);
}

/*function ostatnio(id, nazwa, subs) {
  fetch(new Request('/yt/sas', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/json'
    })
  }), {
    credentials: 'include',
    body: JSON.stringify({
      "id": id,
      "nazwa": nazwa,
      "subs": subs
    })
  })
  .catch(e => console.error)
}
function feczuj() {
  fetch('/yt/sas')
  .then(function(response) {
    return response.json()
  })
  .then(function(ostatnie) {
    var kolej = 0;
    var ostatnielement = 0;
    ostatnie.forEach(el => {
      if(el.id == ostatnielement) return;
      ostatnielement = el.id;
      kolej++;
      if(kolej <= 5) {
        var divek = document.createElement("div")
        divek.className = "karta";
        var divekGlowne = document.createElement("span")
        divekGlowne.className = "glowne";
        divekGlowne.innerHTML = el.nazwa;
        divek.appendChild(divekGlowne);
        var divekInfo = document.createElement("span");
        divekInfo.className = "opis";
        divekInfo.innerHTML = "Subskrybcji: "+el.subs;
        divek.appendChild(divekInfo);
        divek.addEventListener('click', function() {
          zaladuj(el.id, 1, 1);
        })
        document.getElementById("znalezione").appendChild(divek);
        console.log(el.id);
      }
    });
  })
}
*/
function nazwa(query) {
  fetch('https://www.googleapis.com/youtube/v3/search?key='+key+'&part=snippet,id&type=channel&order=viewCount&q='+query)
  .then(function(response) {
    return response.json()
  })
  .then(function(iden) {
    if(iden.pageInfo.totalResults == 0) {
      pokazBlad();
      return;
    }
    zaladuj(iden.items[0].snippet.channelId, 1, 0);
  })
}

function pokazBlad() {
  var komunikat = document.getElementById("blad");

  var id = setInterval(frame, 5);
  var top = -50;

  function frame() {
    if (top == 0) {
      clearInterval(id);
    } else {
      top+=1;
      blad.style.top = top+'px';
    }
  }
  blad.style.display = 'block';
}